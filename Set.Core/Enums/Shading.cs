﻿namespace Set.Core.Enums
{
    public enum Shading
    {
	    Solid,
	    Striped,
	    Outlined
    }
}
