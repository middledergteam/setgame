﻿namespace Set.Core.Enums
{
    public enum Shape
    {
	    Ovals,
	    Squiggles,
	    Diamonds
    }
}
